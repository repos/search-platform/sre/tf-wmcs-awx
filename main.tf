terraform {
  required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}

resource "openstack_compute_instance_v2" "awx_server" {
  count           = var.num_servers
  name            = "deployment-awx-srv${count.index}"
  image_name      = var.image_name
  flavor_name     = var.server_flavor
  security_groups = ["default"]
  metadata = {
    owner = var.owner
  }
  user_data = "{file(${path.module}/cloud-init.sh)}"
  network {
    name = "lan-flat-cloudinstances2b"
  }

}

resource "openstack_compute_instance_v2" "awx_target" {
  count           = var.num_targets
  name            = "deployment-awx-target${count.index}"
  image_name      = var.image_name
  flavor_name     = var.target_flavor
  security_groups = ["default"]
  metadata = {
    owner = var.owner
  }
  user_data = "{file(${path.module}/cloud-init.sh)}"
  network {
    name = "lan-flat-cloudinstances2b"
  }
}

resource "local_file" "ansible_inventory" {
  content = templatefile("${path.root}/ansible/hosts.tmpl", {
    awx_servers   = openstack_compute_instance_v2.awx_server.*.name
    awx_targets   = openstack_compute_instance_v2.awx_target.*.name
    domain_suffix = "${local.dns_domain}"
    } 
  )
  filename        = "ansible/hosts.ini"
  file_permission = "0644"
  # Sleep a few minutes so DNS can propagate, then run playbook. 
  provisioner "local-exec" {
    command = "sleep 300; ansible-playbook -i ansible/hosts.ini  ansible/init.yml"
  } 
}

