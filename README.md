tf-wmcs-awx
=========
Prepare [Ansible AWX](https://github.com/ansible/awx) playground with Terraform and Ansible. 

OK, but what does that mean?
------------
The Terraform code creates the following:
- AWX Server 
- AWX "Targets"- in other words, hosts to converge using AWX.
- Ansible inventory file "hosts.ini"

The Ansible code does the following:
- Install Minikube
- Install Helm

What do I do after that?
--------------
- Install AWX into minikube using the [AWX Operator method](https://github.com/ansible/awx-operator) .  
- Use the target servers to play around with AWX.

More of the install steps will eventually be automated. As it is, after
you run the playbook, visit [the AWX install page](https://github.com/ansible/awx-operator#basic-install) and look for the phrase "Install the manifests by running this" and proceed from there.

How do I run the Ansible playbooks manually?
--------------
Important: the playbooks assume that your local shell username is the same username as 
your WMCS VMs. If this is not the case, you may have to modify the hosts file or 
add the `--user = myuser` flag to your ansible-playbook command (see below). The playbooks also assume that you have passwordless sudo.

- From the repo's root directory, navigate to the ansible subfolder:
``` cd ansible```
- Invoke as follows:
```myhost:ansible$ ansible-playbook init.yml ```


License
-------
Apache 2.0