output "awx_servers" {
  value = [for k, v in openstack_compute_instance_v2.awx_server : "${v.name}.${local.dns_domain}"]

}

output "awx_targets" {
  value = [for k, v in openstack_compute_instance_v2.awx_target : "${v.name}.${local.dns_domain}"]

}